#!/bin/bash

set -Eeou pipefail

# Needed for libgles1-mesa=11.2.0-1ubuntu2
echo 'deb http://ftp.hetzner.de/ubuntu/packages/ xenial universe' >> /etc/apt/sources.list
apt-get update

apt-get download libgles1-mesa-dev:${ARCH}
apt-get download libgles1-mesa:${ARCH}=11.2.0-1ubuntu2

dpkg-deb -R libgles1-mesa-dev_*.deb libgles1-mesa-dev
mv libgles1-mesa-dev/usr/include/GLES /usr/include

dpkg-deb -R libgles1-mesa_*.deb libgles1-mesa
cp libgles1-mesa/usr/lib/*/mesa-egl/libGLESv1_CM.so.1.1.0 /usr/lib/libGLESv1_CM.so

cd "${ROOT}"
make -j4
